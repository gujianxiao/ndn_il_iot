\section{IL Edge computing center}
\subsection{On-line RL based fetching decision}\label{On line RL based fetching decision}
For both model training and usage of IL tasks, data are required. In IoT, the data becomes available gradually over time. Therefore, the edge computing center must determine how many and how frequent to fetch IoT data. Actually, as for IL training, different batch size of the fetched data used has great influence in terms of model accuracy and training times consumption\cite{}. Moreover, due to the distribution of some IoT data may not i.i.d., the higher frequency of the training, the more accurate of the model for current situation. Therefore, it is desired to dynamically make the fetching decision according to in-situ environment.

However, in many scenarios, we have no pre-knowledge of the optimal policy, and the system is restricted by the dynamic network conditions (e.g., bandwidth and data production rate) and user needs (e.g., time consumption, model accuracy). These factors make the problem too difficult to solved by the simple greedy algorithm. In this paper, we use a online reinforcement learning based method, to solve the problem of making the fetching decision.

%Nili uses a monitoring module to collect the environmental feedback, e.g., batch size, training frequency, combing with the network status and user requirements as the input data. After pre-processing, these data will be abstracted as the states of the RL algorithms. Meanwhile, the model accuracy and training time will be treated as the rewards, and the adjustment of the batch size and training frequency are the actions to be operated. Then, RL algorithm will infer the optimal fetching decision based on the states and rewards input.

%Specifically, we implement this idea by using classical RL algorithm: Q-learning. However, Q-learning cannot be used directly in Nili: 1) Q-learning requires a already known reward matrix while we have no pre-knowledge of the rewards of each decision; 2) The computing environment are dynamic. The performance of IL tasks, user's requirements and even the network status might change. Traditional Q-learning cannot reflect this dynamic requirement within it's one time modeling.

Nili solves these challenges by an on-line RL algorithm, as illustrated in Algorithm~\ref{On-line RL based fetching decision}. Since using Q-learning requires a pre-knowledge of actions' rewards matrix, which is not exist when system begins to work, Nili uses the interpolating method to ``fake" a reward matrix, and use an on-line training method to make the optimal action for in-situ environment.
\begin{small}
\begin{algorithm}[htb]
	\caption{On-line RL based fetching decision}
	\label{On-line RL based fetching decision}
	\begin{algorithmic}[1]
		\Require
		Input states $S$, optimization goal $G$, restriction conditions $C$. Interpolating function $f$.
		\Ensure
		Optimal fetching decision;
		\State \textbf{Init notation:} $G$ is to minimize the distance between $G$ and $S$ under $C$, and find the optimal action $A=arg\{min(S,G)|C\}$. $\tilde{s}$: next state, $\tilde{a}$: next action.
		\State \textbf{Procedure begin:}
		\While {!(C reached$\|$A founded)}
		\State Update if $C$ changed and collect new reward $r$;
		\If {!(All real rewards obtained)}
		\State Construct $\tilde{R}$ by $f$ and $r$ via interpolating;
		\EndIf
		\State Calculate Q-table with discount rate $\gamma$:
		\For{state $s$ and action $a$ \textbf{in} state space} 
		\State $Q(s,a)=\tilde{R}(s,a)+\gamma \cdot \underset{\tilde{a}}{max}\{Q(\tilde{s},\tilde{a})\}$;
		\EndFor
		\State Execute the optimal action provided by Q table;
		\EndWhile
	\end{algorithmic}
\end{algorithm}
\end{small}

We use a simple example to show how it works. Assume a real-time scenario that needs to fetch a batch of data for training. The batch size is $b_n$, $n\in [0,3]$. The first round we chose $b_1$ and assume the improvement of model accuracy is 100. Then, we use the piecewise linear interpolation (Nili also supports other methods, see Sec.~\ref{Interest based Function Call}) to construct a ``fake" rewards matrix $\tilde{R}$:
\begin{small}
\begin{align}
\begin{matrix}
&b_0 & b_1 & b_2 & b_3 \\
b_0\ldelim[{4}{0.01cm}&0&\textbf{100}&200&-1 \rdelim]{4}{0.01cm}\\
b_1 &-100&0&100&-1 \\
b_2 &-200&-100&0&-1 \\
b_3 &-300&-200&-100&0 
\end{matrix}
\end{align}
\end{small}
where ``-1" means that operation is not allowed (e.g., cannot increase to 3 due to the limited bandwidth), ``0" means no rewards. Then, we can derive the Q table, i.e., the expected reward of the next actions of all states:
\begin{small}
\begin{align}
\begin{matrix}
&b_0 & b_1 & b_2 & b_3 \\
b_0\ldelim[{4}{0.01cm}&32&36&40&0 \rdelim]{4}{0.01cm}\\
b_1 &12&16&20&0 \\
b_2 &-8&-4&\textbf{0}&0 \\
b_3 &-28&-24&-20&0 
\end{matrix}
\end{align}
\end{small}
According to this Q table, we should choose the action: $b_1$ to $b_2$. And also the action should stay there due to the maximum reward is 0. After a while, we get the real rewards of this action, e.g., 50. And the network allowed us to increase the batch size to 3. Then we can construct $\tilde{R}$ in the similar way. Particularly, the real reward should be kept. The new $\tilde{R}$ is like: 
\begin{small}
\begin{align}
\begin{matrix}
&b_0 & b_1 & b_2 & b_3 \\
b_0\ldelim[{4}{0.01cm}&0&\textbf{100}&150&200 \rdelim]{4}{0.01cm}\\
b_1 &-100&0&\textbf{50}&100 \\
b_2 &-150&-50&0&50 \\
b_3 &-200&-100&-50&0 
\end{matrix}
\end{align}
\end{small}
Similarly, we can get the Q table as follows:
\begin{small}
\begin{align}
\begin{matrix}
&b_0 & b_1 & b_2 & b_3 \\
b_0\ldelim[{4}{0.01cm}&32&36&38&40 \rdelim]{4}{0.01cm}\\
b_1 &12&16&18&20 \\
b_2 &2&6&8&\textbf{10} \\
b_3 &-8&-4&-2&0 
\end{matrix}
\end{align}
\end{small}
Therefore, the action ``$b_2$ to $b_3$" will be executed and state will be kept. Essentially, the proposed online Q-learning is an greedy algorithm. But it has advantages in dealing with big state space, dynamic network environment, and user's specific needs, which frequently happen in the multiple IL environments.

\subsection{Interest based Function Call}\label{Interest based Function Call}
Besides directly operating in the edge computing center, a IL task can be invoked by a command Interest, whose format is as follows:

\begin{footnotesize}
\noindent	\texttt{/<prefix>/<task\_name>/<operation>/<para\_proto>},
\end{footnotesize}
where \texttt{<task\_name>} denotes the name of the IL task, \texttt{<operation>} indicates the operation the user want to operate. Nili supports four operations: run, stop, check, configure. \texttt{<parameter>} contains the parameter of the operation and implemented as a protobuf object.  As for ``run" and ``configure" operation, five important parameters should be given:
\begin{small}
	\begin{flushleft}
		\texttt{message CommandParameter \{\\
			...\\
			optional uint64 OPTIMIZATION = 303;\\
			optional unit64 INTERPOLATING =304;\\
			optional unit64 BATCHTICK = 305\\
			optional uint64 LIFTIME = 306;\\
			optional uint64 PROCESSID =307;\\
			...
			\}	
		} 
	\end{flushleft}
\end{small}
where \texttt{OPTIMIZATION} is used to specify the optimization goal of the task, \texttt{INTERPOLATING} indicates the interpolating method used in the on-line RL algorithm. (Now, Nili supports linear, piecewise linear, and polynomial interpolation), \texttt{BATCHTICK} indicates the interval of the batch size (Nili supports Linear growth and exponential growth for now), \texttt{LIFTIME} and \texttt{PROCESSID} indicates the lifetime of the command and process ID respectively. As for ``check" and ``stop", the process ID must be specified.